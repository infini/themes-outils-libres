Infini theme for [Lutim](https://framagit.org/fiat-tux/hat-softwares/lutim/)

See it live at [https://pic.infini.fr/](https://pic.infini.fr/)

# Credits:

* Based on the work of [nicofrand](https://nicofrand.eu)
* Polished & adapted by b_b for [Infini](https://www.infini.fr/)

# License:

[MIT](https://opensource.org/licenses/MIT)

# Authors:

[nicofrand](https://nicofrand.eu) ([Twitter](https://twitter.com/@nicofrand), [Mastodon](https://mastodon.social/@nicofrand))
[b_b](https://www.eliaz.fr/) ([Mastodon](https://mastodon.social/@b_b))