<!DOCTYPE html>
<html lang="{$locale}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    {if !empty($title)}
        <title>{$title|html} - {$APPLICATION_NAME|html}</title>
    {else}
        <title>{$APPLICATION_NAME|html}</title>
    {/if}
    <meta name="description" content="{__('Generic', 'Framadate is an online service for planning an appointment or make a decision quickly and easily.')}" />

    <link rel="icon" type="image/x-icon" href="{'infini/favicon.ico'|resource}" />
    <link rel="icon" type="image/png" href="{'infini/favicon.png'|resource}">

    <link rel="stylesheet" href="{'css/bootstrap.min.css'|resource}">
    <link rel="stylesheet" href="{'css/datepicker3.css'|resource}">
    <link rel="stylesheet" href="{'css/style.css'|resource}">
    <link rel="stylesheet" href="{'css/frama.css'|resource}">
    <link rel="stylesheet" href="{'infini/css/infini.css'|resource}">
    <link rel="stylesheet" href="{'css/print.css'|resource}" media="print">
    {if $provide_fork_awesome}
        <link rel="stylesheet" href="{'css/fork-awesome.min.css'|resource}">
    {/if}
    <script src="{'js/jquery-3.6.0.min.js'|resource}"></script>
    <script src="{'js/bootstrap.min.js'|resource}"></script>
    <script src="{'js/bootstrap-datepicker.js'|resource}"></script>
    {if 'en' != $locale}
    <script src="{$locale|datepicker_path|resource}"></script>
    {/if}
    <script src="{'js/core.js'|resource}"></script>

    {block name="header"}{/block}

</head>
<body>
<div class="container ombre">

{include file='header.tpl'}

{block name=main}{/block}

</main>
</div> <!-- .container -->
<script src="https://www.infini.fr/spip.php?page=topmenu.js"></script>
</body>
</html>
