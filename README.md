# Thèmes outils libres Infini

Ce dépôt contient les thèmes utilisés par les outils libres proposés par Infini :

- https://bin.infini.fr/
- https://date.infini.fr/
- https://drop.infini.fr/
- https://link.infini.fr/
- https://map.infini.fr/fr/
- https://pad.infini.fr/
- https://pic.infini.fr/

# Déployer les thèmes

Cloner le dépôt et mettre en place des liens symboliques depuis les différents répertoires des outils vers leur thème respectif.

```shell
# sur fra0x
cd /var/www/infini/
git clone https://framagit.org/infini/themes-outils-libres.git themes
```

## date

```shell
cd /var/www/infini/frama/date/framadate
# créer le lien vers le répertoire du thème
ln -s /var/www/infini/themes/date/ infini
# appliquer le patch de config pour prendre en compte le thème
patch -p1 < infini/config_infini.diff
# vide ton cache !
rm tpl_c/*
```

## bin

```shell
cd /var/www/infini/frama/bin/privatebin
# créer le lien vers le répertoire du thème
ln -s /var/www/infini/themes/bin/ infini
# créer le lien vers le template du thème
ln -s ../infini/tpl/infini.php tpl/infini.php
# appliquer le patch de config pour prendre en compte le thème
patch -p1 < infini/config_infini.diff
```

Ajouter notre configuration CSP spécifique pour la topnav :

```
cspheader = "default-src 'none'; base-uri 'self'; form-action 'none'; manifest-src 'self'; connect-src * blob:; script-src 'self' 'unsafe-eval' https://www.infini.fr; style-src 'self' 'unsafe-inline'; font-src 'self'; frame-ancestors 'none'; img-src 'self' data: blob:; media-src blob:; object-src blob:; sandbox allow-same-origin allow-scripts allow-forms allow-modals allow-downloads"
```

## pic

```shell
cd /opt/pic/lutim-0.7.1/themes
# créer le lien vers le répertoire du thème
ln -s /var/www/infini/themes/pic/ infini
```

## drop

```shell
cd /opt/drop/lufi-0.7.1/themes
# créer le lien vers le répertoire du thème
ln -s /var/www/infini/themes/drop/ infini
```

Ajouter notre configuration CSP spécifique pour la topnav :

```
base-uri 'self'; connect-src 'self' wss://drop.infini.fr; default-src 'none'; font-src 'self'; form-action 'self'; frame-ancestors 'none'; img-src 'self' blob: data:; media-src blob:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://www.infini.fr; style-src 'self' 'unsafe-inline'
```

## link

```shell
cd /opt/link/lstu-0.7.1/themes
# créer le lien vers le répertoire du thème
ln -s /var/www/infini/themes/drop/ infini
```

Ajouter notre configuration CSP spécifique pour la topnav :

```
default-src 'none'; script-src 'self' 'unsafe-eval' https://www.infini.fr; style-src 'self' 'unsafe-inline'; img-src 'self' data:; font-src 'self'; form-action 'self'; base-uri 'self'
```

## pad

See https://framagit.org/infini/ep_infini

## map

```shell
# sur map0x
cd /opt
git clone https://framagit.org/infini/themes-outils-libres.git themes
nano /etc/umap/umap.conf
# ajouter ceci au fichier de conf pour que notre thème soit pris en compte automatiquement
# source https://umap-project.readthedocs.io/en/latest/custom/
# Infini template
UMAP_CUSTOM_TEMPLATES = '/opt/themes/map/templates'
UMAP_CUSTOM_STATICS = '/opt/themes/map/static'
```